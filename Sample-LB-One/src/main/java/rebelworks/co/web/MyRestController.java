package rebelworks.co.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author yudhi.saputra@rebelworks.co
 */
@RestController
public class MyRestController {

	@Autowired
	RestTemplate restTemplate;
	
	@RequestMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "Artaban") String name) {
		String greeting = this.restTemplate.getForObject("http://Sample-LB-Two/greeting", String.class);
		return String.format("%s, %s!", greeting, name);
	}
}