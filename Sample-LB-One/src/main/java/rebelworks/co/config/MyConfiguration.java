package rebelworks.co.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AvailabilityFilteringRule;
import com.netflix.loadbalancer.IPing;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.PingUrl;

/**
*
* @author yudhi.saputra@rebelworks.co
*/
public class MyConfiguration {

	@Autowired
	IClientConfig iClientConfig;
	
	@Bean
	public IPing iPing(IClientConfig iClientConfig) {
		return new PingUrl();	
	}
	
	@Bean
	public IRule iRule(IClientConfig iClientConfig) {
		return new AvailabilityFilteringRule();
	}
}