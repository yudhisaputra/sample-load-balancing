package rebelworks.co;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import rebelworks.co.config.MyConfiguration;

/**
 *
 * @author yudhi.saputra@rebelworks.co
 */
@SpringBootApplication
@RibbonClient(name = "Sample-LB-Two", configuration = MyConfiguration.class)
public class Application {

	@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}